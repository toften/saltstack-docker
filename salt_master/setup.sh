#!/bin/bash

key_checker () {

	x=1
	while [ $x -le 250 ]
		do
  		salt-key -A -y
  		x=$(( $x + 1 ))
		sleep 1
	done
	echo "All available keys accepted." && salt "*" test.ping && \
	touch /var/log/salt/master && \
	tail -f /var/log/salt/master

}

consul-template -config /etc/consul-template/salt-master.conf &

export HOME=/home/salt
service salt-master start && key_checker
